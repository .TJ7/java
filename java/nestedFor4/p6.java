/*
 
1
2  3
3  4  5
4  5  6  7    */


import java.util.Scanner;
class TJ7{

	public static void main(String[] ar){

		Scanner obj= new Scanner(System.in);

		System.out.println("Enter the number of rows");
		int row= obj.nextInt();

		System.out.println();

		for(int i=1; i<=row; i++){
			int temp=i;
			for(int j=1; j<=i; j++){
				System.out.print(temp++ +"  ");
			}
			System.out.println();
		}
	}
}


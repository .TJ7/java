/*

F
E 1
D 2 E
C 3 D 4 
B 5 C 6 D
A 7 B 8 C 9   */ 




import java.util.Scanner;
class TJ7{

	public static void main(String[] ard){

		Scanner obj= new Scanner(System.in);

		System.out.println("Enter a number of rows\nnote:- number of rows should be <= 6 for this pattern:");
		int row=obj.nextInt();

		int num=1;

		System.out.println();

		for(int i=1; i<=row; i++){
			int temp=65+row-i;
			for(int j=1; j<=i; j++){
				if(j%2!=0){
					System.out.print((char)temp++ +" ");
				}else{
					System.out.print(num++ +" ");
				}
				
			}
			System.out.println();
		}
	}
}




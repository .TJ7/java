

import java.util.Scanner;
class TJ7{

	public static void main(String[] ar){

		Scanner obj= new Scanner(System.in);

		System.out.println("Enter a number of rows:\nnote:-no of rows should be <=4 for this pattern");
		int row=obj.nextInt();

		int n=row*(row+1)/2;
		int ch=64+n;

		System.out.println();

		for(int i=1; i<=row; i++){
			for(int j=1; j<=i; j++){
				if(i%2!=0){
					System.out.print(n+" ");
				}else{
					System.out.print((char)ch+" ");
				}
				n--;
				ch--;
			}
			System.out.println();
		}
	}
}



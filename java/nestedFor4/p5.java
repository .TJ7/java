/*

A B C D
B C D
C D
D             */
 


import java.util.Scanner;

class TJ7{

	public static void main(String[] a){

		Scanner obj=new Scanner(System.in);

		System.out.println("Enter a number of rows");
		int row=obj.nextInt();

		System.out.println("Enter a char:");
		char ch=obj.next().charAt(0);

		for(int i=1; i<=row; i++){
			char ch1=ch;
			for(int j=1; j<=row-i+1; j++){

				System.out.print(ch1++ +" ");
		}
		ch++;
		System.out.println();
		}
	}
}


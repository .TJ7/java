/*
 
10
10  9
9  8  7
7  6  5  4   */






import java.util.Scanner;

class TJ7{

	public static void main(String[] are){

		Scanner obj= new Scanner(System.in);

		System.out.println("Enter a number of rows");
		int row=obj.nextInt();

		System.out.println("Enter a number :");
		int num=obj.nextInt();

		System.out.println();

		for(int i=1; i<=row; i++){
			for(int j=1; j<=i; j++){
				System.out.print(num-- +"  ");
			}
			num++;
			System.out.println();
		}
	}
}
